---
layout: default
title: release
excerpt: none
---

# fpydocs-4.1.0

- [CHANGELOG]({{ site.baseurl }}/software/CHANGELOG-fpydocs.html#410---2021-08-23)
- [fpydocs-4.1.0.tar.gz]({{ site.baseurl }}/software/fpydocs-4.1.0/fpydocs-4.1.0.tar.gz)
- [SHA512SUM.txt]({{ site.baseurl }}/software/fpydocs-4.1.0/fpydocs-4.1.0.tar.gz.SHA512SUM.txt)
- [SHA256SUM.txt]({{ site.baseurl }}/software/fpydocs-4.1.0/fpydocs-4.1.0.tar.gz.SHA256SUM.txt)
- [signature]({{ site.baseurl }}/software/fpydocs-4.1.0/fpydocs-4.1.0.tar.gz.sig)
- [signing key]({{ site.baseurl }}/pubkeys/pgp_pubkey_since_2019.txt)

back to [software/fpydocs]({{ site.baseurl }}/software/#fpydocs)
