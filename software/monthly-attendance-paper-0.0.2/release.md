---
layout: default
title: release
excerpt: none
---

# monthly-attendance-paper-0.0.2

- [monthly-attendance-paper-0.0.2.tar.gz]({{ site.baseurl }}/software/monthly-attendance-paper-0.0.2/monthly-attendance-paper-0.0.2.tar.gz)
- [SHA512SUM.txt]({{ site.baseurl }}/software/monthly-attendance-paper-0.0.2/monthly-attendance-paper-0.0.2.tar.gz.SHA512SUM.txt)
- [SHA256SUM.txt]({{ site.baseurl }}/software/monthly-attendance-paper-0.0.2/monthly-attendance-paper-0.0.2.tar.gz.SHA256SUM.txt)
- [signature]({{ site.baseurl }}/software/monthly-attendance-paper-0.0.2/monthly-attendance-paper-0.0.2.tar.gz.sig)
- [signing key]({{ site.baseurl }}/pubkeys/pgp_pubkey_since_2019.txt)

back to [software/monthly-attendance-paper]({{ site.baseurl }}/software/#monthly-attendance-paper)
