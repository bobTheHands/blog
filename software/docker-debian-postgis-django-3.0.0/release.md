---
layout: default
title: release
excerpt: none
---

# docker-debian-postgis-django-3.0.0

- [docker-debian-postgis-django-3.0.0.tar.gz]({{ site.baseurl }}/software/docker-debian-postgis-django-3.0.0/docker-debian-postgis-django-3.0.0.tar.gz)
- [SHA512SUM.txt]({{ site.baseurl }}/software/docker-debian-postgis-django-3.0.0/docker-debian-postgis-django-3.0.0.tar.gz.SHA512SUM.txt)
- [SHA256SUM.txt]({{ site.baseurl }}/software/docker-debian-postgis-django-3.0.0/docker-debian-postgis-django-3.0.0.tar.gz.SHA256SUM.txt)
- [signature]({{ site.baseurl }}/software/docker-debian-postgis-django-3.0.0/docker-debian-postgis-django-3.0.0.tar.gz.sig)
- [signing key]({{ site.baseurl }}/pubkeys/pgp_pubkey_since_2019.txt)

back to [software/docker-debian-postgis-django]({{ site.baseurl }}/software/#docker-debian-postgis-django)
