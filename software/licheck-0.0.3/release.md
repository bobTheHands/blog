---
layout: default
title: release
excerpt: none
---

# licheck-0.0.3

- [CHANGELOG]({{ site.baseurl }}/software/CHANGELOG-licheck.html#003---2021-08-10)
- [licheck-0.0.3.tar.gz]({{ site.baseurl }}/software/licheck-0.0.3/licheck-0.0.3.tar.gz)
- [SHA512SUM.txt]({{ site.baseurl }}/software/licheck-0.0.3/licheck-0.0.3.tar.gz.SHA512SUM.txt)
- [SHA256SUM.txt]({{ site.baseurl }}/software/licheck-0.0.3/licheck-0.0.3.tar.gz.SHA256SUM.txt)
- [signature]({{ site.baseurl }}/software/licheck-0.0.3/licheck-0.0.3.tar.gz.sig)
- [signing key]({{ site.baseurl }}/pubkeys/pgp_pubkey_since_2019.txt)

## pypi.org

### licheck-0.0.3-py3-none-any.whl

- [SHA256SUM.txt]({{ site.baseurl }}/software/licheck-0.0.3/licheck-0.0.3-py3-none-any.whl.SHA256SUM.txt)
- [MD5SUM.txt]({{ site.baseurl }}/software/licheck-0.0.3/licheck-0.0.3-py3-none-any.whl.MD5SUM.txt)

back to [software/licheck]({{ site.baseurl }}/software/#licheck)
