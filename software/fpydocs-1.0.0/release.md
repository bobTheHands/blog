---
layout: default
title: release
excerpt: none
---

# fpydocs-1.0.0

- [fpydocs-1.0.0.tar.gz]({{ site.baseurl }}/software/fpydocs-1.0.0/fpydocs-1.0.0.tar.gz)
- [SHA512SUM.txt]({{ site.baseurl }}/software/fpydocs-1.0.0/fpydocs-1.0.0.tar.gz.SHA512SUM.txt)
- [SHA256SUM.txt]({{ site.baseurl }}/software/fpydocs-1.0.0/fpydocs-1.0.0.tar.gz.SHA256SUM.txt)
- [signature]({{ site.baseurl }}/software/fpydocs-1.0.0/fpydocs-1.0.0.tar.gz.sig)
- [signing key]({{ site.baseurl }}/pubkeys/pgp_pubkey_since_2019.txt)

back to [software/fpydocs]({{ site.baseurl }}/software/#fpydocs)
