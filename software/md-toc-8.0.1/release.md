---
layout: default
title: release
excerpt: none
---

# md-toc-8.0.1

- [CHANGELOG]({{ site.baseurl }}/software/CHANGELOG-md-toc.html#801---2021-08-20)
- [md-toc-8.0.1.tar.gz]({{ site.baseurl }}/software/md-toc-8.0.1/md-toc-8.0.1.tar.gz)
- [SHA512SUM.txt]({{ site.baseurl }}/software/md-toc-8.0.1/md-toc-8.0.1.tar.gz.SHA512SUM.txt)
- [SHA256SUM.txt]({{ site.baseurl }}/software/md-toc-8.0.1/md-toc-8.0.1.tar.gz.SHA256SUM.txt)
- [signature]({{ site.baseurl }}/software/md-toc-8.0.1/md-toc-8.0.1.tar.gz.sig)
- [signing key]({{ site.baseurl }}/pubkeys/pgp_pubkey_since_2019.txt)

back to [software/md-toc]({{ site.baseurl }}/software/#md-toc)
