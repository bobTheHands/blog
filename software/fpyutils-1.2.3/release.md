---
layout: default
title: release
excerpt: none
---

# fpyutils-1.2.3

- [fpyutils-1.2.3.tar.gz]({{ site.baseurl }}/software/fpyutils-1.2.3/fpyutils-1.2.3.tar.gz)
- [SHA512SUM.txt]({{ site.baseurl }}/software/fpyutils-1.2.3/fpyutils-1.2.3.tar.gz.SHA512SUM.txt)
- [SHA256SUM.txt]({{ site.baseurl }}/software/fpyutils-1.2.3/fpyutils-1.2.3.tar.gz.SHA256SUM.txt)
- [signature]({{ site.baseurl }}/software/fpyutils-1.2.3/fpyutils-1.2.3.tar.gz.sig)
- [signing key]({{ site.baseurl }}/pubkeys/pgp_pubkey_since_2019.txt)

back to [software/fpyutils]({{ site.baseurl }}/software/#fpyutils)
