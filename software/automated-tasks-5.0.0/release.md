---
layout: default
title: release
excerpt: none
---

# automated-tasks-5.0.0

- [automated-tasks-5.0.0.tar.gz]({{ site.baseurl }}/software/automated-tasks-5.0.0/automated-tasks-5.0.0.tar.gz)
- [SHA512SUM.txt]({{ site.baseurl }}/software/automated-tasks-5.0.0/automated-tasks-5.0.0.tar.gz.SHA512SUM.txt)
- [SHA256SUM.txt]({{ site.baseurl }}/software/automated-tasks-5.0.0/automated-tasks-5.0.0.tar.gz.SHA256SUM.txt)
- [signature]({{ site.baseurl }}/software/automated-tasks-5.0.0/automated-tasks-5.0.0.tar.gz.sig)
- [signing key]({{ site.baseurl }}/pubkeys/pgp_pubkey_since_2019.txt)

back to [software/automated-tasks]({{ site.baseurl }}/software/#automated-tasks)
