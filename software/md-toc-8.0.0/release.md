---
layout: default
title: release
excerpt: none
---

# md-toc-8.0.0

- [CHANGELOG]({{ site.baseurl }}/software/CHANGELOG-md-toc.html#800---2021-05-28)
- [md-toc-8.0.0.tar.gz]({{ site.baseurl }}/software/md-toc-8.0.0/md-toc-8.0.0.tar.gz)
- [SHA512SUM.txt]({{ site.baseurl }}/software/md-toc-8.0.0/md-toc-8.0.0.tar.gz.SHA512SUM.txt)
- [SHA256SUM.txt]({{ site.baseurl }}/software/md-toc-8.0.0/md-toc-8.0.0.tar.gz.SHA256SUM.txt)
- [signature]({{ site.baseurl }}/software/md-toc-8.0.0/md-toc-8.0.0.tar.gz.sig)
- [signing key]({{ site.baseurl }}/pubkeys/pgp_pubkey_since_2019.txt)

back to [software/md-toc]({{ site.baseurl }}/software/#md-toc)
