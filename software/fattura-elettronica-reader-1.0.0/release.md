---
layout: default
title: release
excerpt: none
---

# fattura-elettronica-reader-1.0.0

- [fattura-elettronica-reader-1.0.0.tar.gz]({{ site.baseurl }}/software/fattura-elettronica-reader-1.0.0/fattura-elettronica-reader-1.0.0.tar.gz)
- [SHA512SUM.txt]({{ site.baseurl }}/software/fattura-elettronica-reader-1.0.0/fattura-elettronica-reader-1.0.0.tar.gz.SHA512SUM.txt)
- [SHA256SUM.txt]({{ site.baseurl }}/software/fattura-elettronica-reader-1.0.0/fattura-elettronica-reader-1.0.0.tar.gz.SHA256SUM.txt)
- [signature]({{ site.baseurl }}/software/fattura-elettronica-reader-1.0.0/fattura-elettronica-reader-1.0.0.tar.gz.sig)
- [signing key]({{ site.baseurl }}/pubkeys/pgp_pubkey_since_2019.txt)

back to [software/fattura-elettronica-reader]({{ site.baseurl }}/software/#fattura-elettronica-reader)
