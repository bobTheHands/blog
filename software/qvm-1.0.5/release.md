---
layout: default
title: release
excerpt: none
---

# qvm-1.0.5

- [qvm-1.0.5.tar.gz]({{ site.baseurl }}/software/qvm-1.0.5/qvm-1.0.5.tar.gz)
- [SHA512SUM.txt]({{ site.baseurl }}/software/qvm-1.0.5/qvm-1.0.5.tar.gz.SHA512SUM.txt)
- [SHA256SUM.txt]({{ site.baseurl }}/software/qvm-1.0.5/qvm-1.0.5.tar.gz.SHA256SUM.txt)
- [signature]({{ site.baseurl }}/software/qvm-1.0.5/qvm-1.0.5.tar.gz.sig)
- [signing key]({{ site.baseurl }}/pubkeys/pgp_pubkey_since_2019.txt)

back to [software/qvm]({{ site.baseurl }}/software/#qvm)
