---
layout: default
title: release
excerpt: none
---

# fpyutils-2.0.0

- [CHANGELOG]({{ site.baseurl }}/software/CHANGELOG-fpyutils.html#200---2021-04-26)
- [fpyutils-2.0.0.tar.gz]({{ site.baseurl }}/software/fpyutils-2.0.0/fpyutils-2.0.0.tar.gz)
- [SHA512SUM.txt]({{ site.baseurl }}/software/fpyutils-2.0.0/fpyutils-2.0.0.tar.gz.SHA512SUM.txt)
- [SHA256SUM.txt]({{ site.baseurl }}/software/fpyutils-2.0.0/fpyutils-2.0.0.tar.gz.SHA256SUM.txt)
- [signature]({{ site.baseurl }}/software/fpyutils-2.0.0/fpyutils-2.0.0.tar.gz.sig)
- [signing key]({{ site.baseurl }}/pubkeys/pgp_pubkey_since_2019.txt)

back to [software/fpyutils]({{ site.baseurl }}/software/#fpyutils)
