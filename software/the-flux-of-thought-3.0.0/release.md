---
layout: default
title: release
excerpt: none
---

# the-flux-of-thought-3.0.0

- [the-flux-of-thought-3.0.0.tar.gz]({{ site.baseurl }}/software/the-flux-of-thought-3.0.0/the-flux-of-thought-3.0.0.tar.gz)
- [SHA512SUM.txt]({{ site.baseurl }}/software/the-flux-of-thought-3.0.0/the-flux-of-thought-3.0.0.tar.gz.SHA512SUM.txt)
- [SHA256SUM.txt]({{ site.baseurl }}/software/the-flux-of-thought-3.0.0/the-flux-of-thought-3.0.0.tar.gz.SHA256SUM.txt)
- [signature]({{ site.baseurl }}/software/the-flux-of-thought-3.0.0/the-flux-of-thought-3.0.0.tar.gz.sig)
- [signing key]({{ site.baseurl }}/pubkeys/pgp_pubkey_since_2019.txt)

back to [software/the-flux-of-thought]({{ site.baseurl }}/software/#the-flux-of-thought)
