---
layout: default
title: release
excerpt: none
---

# licheck-0.0.2

- [CHANGELOG]({{ site.baseurl }}/software/CHANGELOG-licheck.html#002---2021-08-08)
- [licheck-0.0.2.tar.gz]({{ site.baseurl }}/software/licheck-0.0.2/licheck-0.0.2.tar.gz)
- [SHA512SUM.txt]({{ site.baseurl }}/software/licheck-0.0.2/licheck-0.0.2.tar.gz.SHA512SUM.txt)
- [SHA256SUM.txt]({{ site.baseurl }}/software/licheck-0.0.2/licheck-0.0.2.tar.gz.SHA256SUM.txt)
- [signature]({{ site.baseurl }}/software/licheck-0.0.2/licheck-0.0.2.tar.gz.sig)
- [signing key]({{ site.baseurl }}/pubkeys/pgp_pubkey_since_2019.txt)

## pypi.org

### licheck-0.0.2-py3-none-any.whl

- [SHA256SUM.txt]({{ site.baseurl }}/software/licheck-0.0.2/licheck-0.0.2-py3-none-any.whl.SHA256SUM.txt)
- [MD5SUM.txt]({{ site.baseurl }}/software/licheck-0.0.2/licheck-0.0.2-py3-none-any.whl.MD5SUM.txt)

back to [software/licheck]({{ site.baseurl }}/software/#licheck)
