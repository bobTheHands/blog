---
layout: default
title: release
excerpt: none
---

# fpyutils-2.0.1

- [CHANGELOG]({{ site.baseurl }}/software/CHANGELOG-fpyutils.html#201---2021-07-30)
- [fpyutils-2.0.1.tar.gz]({{ site.baseurl }}/software/fpyutils-2.0.1/fpyutils-2.0.1.tar.gz)
- [SHA512SUM.txt]({{ site.baseurl }}/software/fpyutils-2.0.1/fpyutils-2.0.1.tar.gz.SHA512SUM.txt)
- [SHA256SUM.txt]({{ site.baseurl }}/software/fpyutils-2.0.1/fpyutils-2.0.1.tar.gz.SHA256SUM.txt)
- [signature]({{ site.baseurl }}/software/fpyutils-2.0.1/fpyutils-2.0.1.tar.gz.sig)
- [signing key]({{ site.baseurl }}/pubkeys/pgp_pubkey_since_2019.txt)

back to [software/fpyutils]({{ site.baseurl }}/software/#fpyutils)
