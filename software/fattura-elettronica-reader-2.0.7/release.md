---
layout: default
title: release
excerpt: none
---

# fattura-elettronica-reader-2.0.7

- [CHANGELOG]({{ site.baseurl }}/software/CHANGELOG-fattura-elettronica-reader.html#207---2021-07-31)
- [fattura-elettronica-reader-2.0.7.tar.gz]({{ site.baseurl }}/software/fattura-elettronica-reader-2.0.7/fattura-elettronica-reader-2.0.7.tar.gz)
- [SHA512SUM.txt]({{ site.baseurl }}/software/fattura-elettronica-reader-2.0.7/fattura-elettronica-reader-2.0.7.tar.gz.SHA512SUM.txt)
- [SHA256SUM.txt]({{ site.baseurl }}/software/fattura-elettronica-reader-2.0.7/fattura-elettronica-reader-2.0.7.tar.gz.SHA256SUM.txt)
- [signature]({{ site.baseurl }}/software/fattura-elettronica-reader-2.0.7/fattura-elettronica-reader-2.0.7.tar.gz.sig)
- [signing key]({{ site.baseurl }}/pubkeys/pgp_pubkey_since_2019.txt)

## pypi.org

### fattura-elettronica-reader-2.0.7-py3-none-any.whl

- [SHA256SUM.txt]({{ site.baseurl }}/software/fattura-elettronica-reader-2.0.7/fattura_elettronica_reader-2.0.7-py3-none-any.whl.SHA256SUM.txt)
- [MD5SUM.txt]({{ site.baseurl }}/software/fattura-elettronica-reader-2.0.7/fattura_elettronica_reader-2.0.7-py3-none-any.whl.MD5SUM.txt)

back to [software/fattura-elettronica-reader]({{ site.baseurl }}/software/#fattura-elettronica-reader)
