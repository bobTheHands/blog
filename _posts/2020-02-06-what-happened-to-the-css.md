---
title: What happened to the CSS?
tags: [CSS, website, blog]
updated: 2020-02-06 20:10
description: Reasons for the removal of the CSS from this website
---

<!--excerpt_start-->
Yes, I practically removed all the CSS file from this website!
<!--excerpt_end-->

This gives me some advantages:

- I can now concentrate more on the structure and on the content
- I don't spend my time looking for solutions to do x, y or z in CSS
- the website is faster to load

If you have problems because of the absence of a dark theme, don't complain: install [Redshift](http://jonls.dk/redshift/).

~

Cheers!
