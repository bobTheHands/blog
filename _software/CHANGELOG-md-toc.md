---
layout: default
title: md-toc releases
excerpt: none
updated: 2021-08-20 15:37:47
date: 2021-08-20 15:37:47
last_version: 8.0.1
---

<!--last_release_start-->
# [md-toc]({{ site.baseurl }}/software/#md-toc) changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Only the last version and unreleased changes are reported
in the RSS feeds.

## Unreleased

Empty

## [8.0.1] - 2021-08-20

### Fixed

- Cmark-specific code is now more adherent to the original.
- License references are now more accurate.
- The PyPI wheel is now made reproducible.
- Updated email.

### Changed

- Cmark-specific code has been moved to a separate Python module: `cmark.py`.

### Added

- New git hooks have been added in the pre-commit file.
- Examples for unit tests of Cmark 0.30 have been checked with the existing examples.
<!--last_release_end-->

## [8.0.0] - 2021-05-28

### Changed

- Translated functions directly from cmark to treat emphasis.
- `get_md_header` and `get_atx_heading` now support multiple lines.
- Updated documentation.
- Updated tests.
- Updated package dependencies.

### Added

- Newline string argument choice from the CLI.
- Added these exceptions:
  - `StringCannotContainNewlines`
  - `CannotTreatUnicodeString`

### Removed

- Removal of these functions:
  - `get_generic_fdr_indices`
  - `get_fdr_indices`
  - `can_open_emphasis`
  - `can_close_emphasis`
  - `get_nearest_list_id`
