---
layout: default
title: fpyutils releases
excerpt: none
updated: 2021-07-30 16:01:52
date: 2021-07-30 16:01:52
last_version: 2.0.1
---

<!--last_release_start-->
# [fpyutils]({{ site.baseurl }}/software/#fpyutils) changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Only the last version and unreleased changes are reported
in the RSS feeds.

## Unreleased

Empty

## [2.0.1] - 2021-07-30

### Added

- New pre-commit hooks.

### Changed

- Changed assertions to exceptions.
- Updated package dependencies.
- Updated URLs.
<!--last_release_end-->

## [2.0.0] - 2021-04-26

### Changed

- Updated tests.
- Updated package dependencies.

### Removed

- Removal of these functions:
  - `load_configuration`
- Removal of these modules:
  - `yaml.py`
