---
layout: default
title: fpydocs releases
excerpt: none
updated: 2021-08-23 17:02:52
date: 2021-08-23 17:02:52
last_version: 4.1.0
---

<!--last_release_start-->
# [fpydocs]({{ site.baseurl }}/software/#fpydocs) changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Only the last version and unreleased changes are reported
in the RSS feeds.

## Unreleased

Empty

## [4.1.0] - 2021-08-23

### Changed

- Improved existing blueprints.

### Added

- Added and updated some pre-commit hooks.
<!--last_release_end-->
