---
layout: default
title: licheck releases
excerpt: none
updated: 2021-08-10 20:56:19
date: 2021-08-10 20:56:19
last_version: 0.0.3
---

<!--last_release_start-->
# [licheck]({{ site.baseurl }}/software/#licheck) changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Only the last version and unreleased changes are reported
in the RSS feeds.

## Unreleased

Empty

## [0.0.3] - 2021-08-10

### Fixed

- Cache from different repositories is now preserved.
<!--last_release_end-->

## [0.0.2] - 2021-08-08

### Added

- Documentation concerning configuration files.

## [0.0.1] - 2021-08-06

### Added

- First release.
