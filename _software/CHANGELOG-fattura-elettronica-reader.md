---
layout: default
title: fattura-elettronica-reader releases
excerpt: none
updated: 2021-07-21 20:40:26
date: 2021-07-21 20:40:26
last_version: 2.0.7
---

<!--last_release_start-->
# [fattura-elettronica-reader]({{ site.baseurl }}/software/#fattura-elettronica-reader) changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Only the last version and unreleased changes are reported
in the RSS feeds.

## Unreleased

Empty

## [2.0.7] - 2021-07-31

### Changed

- Updated file checksum variable.
- Updated readme.
- Added git hooks.
- Updated package dependencies.
<!--last_release_end-->

## [2.0.6] - 2021-07-05

### Changed

- Updated file checksum variable.
- Updated readme.
