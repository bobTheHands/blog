---
layout: page
title: About
permalink: /about/
---

## Important

This website is also available on tor: [usggwz6vwth276fdyccskoba5u2jis5ugv2ew2fy2blowgf2dv73axad.onion](http://usggwz6vwth276fdyccskoba5u2jis5ugv2ew2fy2blowgf2dv73axad.onion)

## Presentation

This blog tells my experiences with [free software](https://www.gnu.org/philosophy/free-sw.en.html),
[free hardware](https://www.gnu.org/philosophy/free-hardware-designs.en.html), computing, "hacks",
technology and whatever comes to my mind I want to share.

I hope this turns out to be a useful blog for anyone who encounters
similar computer science problems.

## Comment rules

Every post (and some other pages like this one) contains a comment section which
is just a `mailto` link to one of my emails.

When you send me an email I will report certain elements
verbatim, such as:
- the subject
- the sending date
- your name, if present, or your email if you request so in the comment. If
  none is available I will simply use `anonymous` in this field.
- the content which can be written in markdown

## Contacts

You will find me on:

- [my canonical repositories](https://software.franco.net.eu.org/)
- [Codeberg](https://codeberg.org/frnmst)
- [GitHub](https://github.com/frnmst) (abandonment phase)
- [mail one](mailto://franco.masotti@tutanota.com)
- [mail two](mailto://franco.masotti.1@protonmail.com)
- [ORCID iD](https://orcid.org/0000-0002-1736-3858)

and other less relevant web locations.

You won't find me on social networks.

## My PGP public key

[PGP public key]({{ site.baseurl }}/pubkeys/pgp_pubkey_since_2019.txt)

## Software

See the [software]({{ site.baseurl }}/software/) page.
