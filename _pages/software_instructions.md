---
layout: page
title: Software instructions
permalink: /software/instructions/
---

## Table of contents

<!--TOC-->

- [Table of contents](#table-of-contents)
- [Terminology](#terminology)
- [Methods](#methods)
  - [Upload](#upload)
    - [Get the variables](#get-the-variables)
    - [Create an archive](#create-an-archive)
    - [Signing](#signing)
    - [Checksums](#checksums)
    - [Python project on PyPI](#python-project-on-pypi)
    - [Update the entry](#update-the-entry)
    - [Create a new release file](#create-a-new-release-file)
    - [Update the changelog](#update-the-changelog)
    - [Update the table of contents](#update-the-table-of-contents)
  - [Download](#download)
    - [Get the public key](#get-the-public-key)
    - [Download the repository](#download-the-repository)
    - [Check the signature](#check-the-signature)
    - [Run the checksums](#run-the-checksums)
    - [Extract](#extract)
    - [Python project on PyPI](#python-project-on-pypi-1)

<!--TOC-->

## Terminology

- `project_dir`: the full path directory of the project
- `project`: the project name
- `project_python_module`: the python module name of the project (e.g: md-toc is md_toc)
- `tag`: the git tag name which is usually [semver](https://semver.org/)ed
- `signing_key`: the public key file used to sign the archive file
- `changelog_slugified_header`: the slugified header corresponding to a tag in a changelog file
- `url`: a generic url
- `PyPI_download_page`: the URL of the download page of the package on PyPI

## Methods

### Upload

What follows are the steps I use to upload the software on this page.

#### Get the variables

Go into the project directory and then:

    export project_dir="$(pwd)"
    export project="$(basename "$(pwd)")"
    export tag="$(git tag | sort --human-numeric-sort --ignore-leading-blanks | tail --lines=1)"

#### Create an archive

    cd /tmp
    git -C ${project_dir} archive --format=tar.gz --output=/tmp/${project}-${tag}.tar.gz --prefix=${project}-${tag}/ ${tag}

#### Signing

    gpg --armor --output ${project}-${tag}.tar.gz.sig --detach-sig ${project}-${tag}.tar.gz

#### Checksums

    sha512sum ${project}-${tag}.tar.gz > ${project}-${tag}.tar.gz.SHA512SUM.txt
    sha256sum ${project}-${tag}.tar.gz > ${project}-${tag}.tar.gz.SHA256SUM.txt

#### Python project on PyPI

    make dist
    cd dist
    sha256sum ${project_python_module}-${tag}-py3-none-any.whl > ${project_python_module}-${tag}-py3-none-any.whl.SHA256SUM.txt
    md5sum ${project_python_module}-${tag}-py3-none-any.whl > ${project_python_module}-${tag}-py3-none-any.whl.MD5SUM.txt

#### Update the entry

Create a new entry in this file

```
- [`${tag}`]({{ site.baseurl }}/software/${project}-${tag}/release.html)
```

#### Create a new release file

Create a new file called `${project}-${tag}/release.md` and add the following:

```
---
layout: default
title: release
excerpt: none
---

# ${project}-${tag}

- [CHANGELOG]({{ site.baseurl }}/software/CHANGELOG-${project}.html#${changelog_slugified_header})
- [${project}-${tag}.tar.gz]({{ site.baseurl }}/software/${project}-${tag}.tar.gz)
- [SHA512SUM.txt]({{ site.baseurl }}/software/${project}-${tag}.tar.gz.SHA512SUM.txt)
- [SHA256SUM.txt]({{ site.baseurl }}/software/${project}-${tag}.tar.gz.SHA256SUM.txt)
- [signature]({{ site.baseurl }}/software/${project}-${tag}.tar.gz.sig)
- [signing key]({{ site.baseurl }}/pubkeys/${signing_key})
```

#### Update the changelog

Update the changelog file at `_software/CHANGELOG-${project}.md`:

- update the front matter with the appripriate data:
  - `updated` and `date` must be the UTC representation of the timestamp of the
    git commit corresponding to the last published version
  - `last_version` must correspond to the git tag used for the last published version
- move the `<!--last_release_end-->` HTML comment after the last line of the new version
  changelog

#### Update the table of contents

    md_toc -p github -l 6 software.md

### Download

Run the following to download and verify the software.

#### Get the public key

If the public key is unknown you must import it from a trusted source:

    cd /tmp
    wget "${public_key_url}"
    gpg --import "${public_key_file}"

#### Download the repository

    cd /tmp
    wget ${url}/${project}-${tag}.tar.gz.sig

#### Check the signature

    wget ${url}/${project}-${tag}.tar.gz
    gpg --verify ${project}-${tag}.tar.gz.sig

#### Run the checksums

    sha512sum --check ${project}-${tag}.tar.gz.SHA512SUM.txt
    sha256sum --check ${project}-${tag}.tar.gz.SHA256SUM.txt

#### Extract

    tar -xvzf ${project}-${tag}.tar.gz

#### Python project on PyPI

    wget ${PyPI_download_page}/${project_python_module}-${tag}-py3-none-any.whl
    sha256sum --check ${project_python_module}-${tag}-py3-none-any.whl.SHA256SUM.txt
    md5sum --check ${project_python_module}-${tag}-py3-none-any.whl.MD5SUM.txt
