---
layout: page
title: Software
permalink: /software/
---

## Table of contents

<!--TOC-->

- [Table of contents](#table-of-contents)
- [Introduction](#introduction)
  - [Extract](#extract)
- [Signing keys](#signing-keys)
- [Instructions](#instructions)
- [Software](#software)
  - [automated-tasks](#automated-tasks)
    - [Repository](#repository)
    - [Documentation](#documentation)
    - [Releases](#releases)
  - [django-futils](#django-futils)
    - [Repository](#repository-1)
    - [Documentation](#documentation-1)
    - [Releases](#releases-1)
  - [docker-debian-postgis-django](#docker-debian-postgis-django)
    - [Repository](#repository-2)
    - [Documentation](#documentation-2)
    - [Releases](#releases-2)
  - [fattura-elettronica-reader](#fattura-elettronica-reader)
    - [Repository](#repository-3)
    - [Documentation](#documentation-3)
    - [Releases](#releases-3)
  - [fpydocs](#fpydocs)
    - [Repository](#repository-4)
    - [Documentation](#documentation-4)
    - [Releases](#releases-4)
  - [fpyutils](#fpyutils)
    - [Documentation](#documentation-5)
    - [Repository](#repository-5)
    - [Releases](#releases-5)
  - [licheck](#licheck)
    - [Repository](#repository-6)
    - [Documentation](#documentation-6)
    - [Releases](#releases-6)
  - [md-toc](#md-toc)
    - [Repository](#repository-7)
    - [Documentation](#documentation-7)
    - [Releases](#releases-7)
  - [monthly-attendance-paper](#monthly-attendance-paper)
    - [Repository](#repository-8)
    - [Documentation](#documentation-8)
    - [Releases](#releases-8)
  - [the-flux-of-thought](#the-flux-of-thought)
    - [Repository](#repository-9)
    - [Documentation](#documentation-9)
    - [Releases](#releases-9)
  - [qvm](#qvm)
    - [Repository](#repository-10)
    - [Documentation](#documentation-10)
    - [Releases](#releases-10)

<!--TOC-->

## Introduction

This page is the only *real* trusted source of some of my software, publicly available on the Internet.

Here you will find methods to assert the authenticity of the presented software packages.

You may contact me directly to obtain the public key fingerprint in a different way.

### Extract

The following extract is from a [post by Mike Gerwitz](https://mikegerwitz.com/2012/05/a-git-horror-story-repository-integrity-with-signed-commits#trust):

> Git Host
>>
>>    Git hosting providers are probably the most easily overlooked trustees—providers like Gitorious, GitHub, Bitbucket, SourceForge, Google Code, etc. Each provides hosting for your repository and “secures” it by allowing only you, or other authorized users, to push to it, often with the use of SSH keys tied to an account. By using a host as the primary holder of your repository—the repository from which most clone and push to—you are entrusting them with the entirety of your project; you are stating, “Yes, I trust that my source code is safe with you and will not be tampered with”. This is a dangerous assumption. Do you trust that your host properly secures your account information? Furthermore, bugs exist in all but the most trivial pieces of software, so what is to say that there is not a vulnerability just waiting to be exploited in your host’s system, completely compromising your repository?
>>
>>    It was not too long ago (March 4th, 2012) that a public key security vulnerability at GitHub was exploited by a Russian man named Egor Homakov, allowing him to successfully commit to the master branch of the Ruby on Rails framework repository hosted on GitHub. Oops.

    Copyright © 2019 Mike Gerwitz. Licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.

## Signing keys

| Key | Fingerprint |
|-----|-------------|
| [pgp_pubkey_since_2019.txt]({{ site.baseurl }}/pubkeys/pgp_pubkey_since_2019.txt) | [pgp_pubkey_fingerprint_since_2019.txt]({{ site.baseurl }}/pubkeys/pgp_pubkey_fingerprint_since_2019.txt) |

## Instructions

[instructions]({{ site.baseurl }}/software/instructions)

## Software

### automated-tasks

status: active

#### Repository

- [canonical repository](https://software.franco.net.eu.org/frnmst/automated-tasks)
- [Codeberg](https://codeberg.org/frnmst/automated-tasks)
- [Framagit](https://framagit.org/frnmst/automated-tasks)

#### Documentation

- [docs.franco.net.eu.org/automated-tasks](https://docs.franco.net.eu.org/automated-tasks)

#### Releases

- [`11.1.0`]({{ site.baseurl }}/software/automated-tasks-11.1.0/release.html)
- [`11.0.0`]({{ site.baseurl }}/software/automated-tasks-11.0.0/release.html)
- [`10.1.0`]({{ site.baseurl }}/software/automated-tasks-10.1.0/release.html)
- [`10.0.0`]({{ site.baseurl }}/software/automated-tasks-10.0.0/release.html)
- [`9.0.0`]({{ site.baseurl }}/software/automated-tasks-9.0.0/release.html)
- [`8.0.2`]({{ site.baseurl }}/software/automated-tasks-8.0.2/release.html)
- [`8.0.1`]({{ site.baseurl }}/software/automated-tasks-8.0.1/release.html)
- [`8.0.0`]({{ site.baseurl }}/software/automated-tasks-8.0.0/release.html)
- [`7.0.1`]({{ site.baseurl }}/software/automated-tasks-7.0.1/release.html)
- [`7.0.0`]({{ site.baseurl }}/software/automated-tasks-7.0.0/release.html)
- [`6.0.0`]({{ site.baseurl }}/software/automated-tasks-6.0.0/release.html)
- [`5.0.0`]({{ site.baseurl }}/software/automated-tasks-5.0.0/release.html)

---

### django-futils

status: active

#### Repository

- [canonical repository](https://software.franco.net.eu.org/frnmst/django-futils)
- [Codeberg](https://codeberg.org/frnmst/django-futils)
- [Framagit](https://framagit.org/frnmst/django-futils)

#### Documentation

- [docs.franco.net.eu.org/django-futils](https://docs.franco.net.eu.org/django-futils)

#### Releases

- [`3.0.0`]({{ site.baseurl }}/software/django-futils-3.0.0/release.html)
- [`2.0.0`]({{ site.baseurl }}/software/django-futils-2.0.0/release.html)
- [`1.0.0`]({{ site.baseurl }}/software/django-futils-1.0.0/release.html)
- [`0.1.0`]({{ site.baseurl }}/software/django-futils-0.1.0/release.html)
- [`0.0.2`]({{ site.baseurl }}/software/django-futils-0.0.2/release.html)
- [`0.0.1`]({{ site.baseurl }}/software/django-futils-0.0.1/release.html)

---

### docker-debian-postgis-django

status: active

#### Repository

- [canonical repository](https://software.franco.net.eu.org/frnmst/docker-debian-postgis-django)
- [Codeberg](https://codeberg.org/frnmst/docker-debian-postgis-django)
- [Framagit](https://framagit.org/frnmst/docker-debian-postgis-django)

#### Documentation

- [software.franco.net.eu.org/frnmst/docker-debian-postgis-django#docker-debian-postgis-django](https://software.franco.net.eu.org/frnmst/docker-debian-postgis-django#docker-debian-postgis-django)

#### Releases

- [`5.0.0`]({{ site.baseurl }}/software/docker-debian-postgis-django-5.0.0/release.html)
- [`4.0.0`]({{ site.baseurl }}/software/docker-debian-postgis-django-4.0.0/release.html)
- [`3.0.1`]({{ site.baseurl }}/software/docker-debian-postgis-django-3.0.1/release.html)
- [`3.0.0`]({{ site.baseurl }}/software/docker-debian-postgis-django-3.0.0/release.html)
- [`2.0.2`]({{ site.baseurl }}/software/docker-debian-postgis-django-2.0.2/release.html)
- [`2.0.1`]({{ site.baseurl }}/software/docker-debian-postgis-django-2.0.1/release.html)
- [`2.0.0`]({{ site.baseurl }}/software/docker-debian-postgis-django-2.0.0/release.html)
- [`1.0.0`]({{ site.baseurl }}/software/docker-debian-postgis-django-1.0.0/release.html)
- [`0.0.4`]({{ site.baseurl }}/software/docker-debian-postgis-django-0.0.4/release.html)
- [`0.0.3`]({{ site.baseurl }}/software/docker-debian-postgis-django-0.0.3/release.html)
- [`0.0.2`]({{ site.baseurl }}/software/docker-debian-postgis-django-0.0.2/release.html)
- [`0.0.1`]({{ site.baseurl }}/software/docker-debian-postgis-django-0.0.1/release.html)

---

### fattura-elettronica-reader

status: active

#### Repository

- [canonical repository](https://software.franco.net.eu.org/frnmst/fattura-elettronica-reader)
- [Codeberg](https://codeberg.org/frnmst/fattura-elettronica-reader)
- [Framagit](https://framagit.org/frnmst/fattura-elettronica-reader)

#### Documentation

- [docs.franco.net.eu.org/fattura-elettronica-reader](https://docs.franco.net.eu.org/fattura-elettronica-reader)

#### Releases

- [`2.0.7`]({{ site.baseurl }}/software/fattura-elettronica-reader-2.0.7/release.html)
- [`2.0.6`]({{ site.baseurl }}/software/fattura-elettronica-reader-2.0.6/release.html)
- [`2.0.5`]({{ site.baseurl }}/software/fattura-elettronica-reader-2.0.5/release.html)
- [`2.0.4`]({{ site.baseurl }}/software/fattura-elettronica-reader-2.0.4/release.html)
- [`2.0.3`]({{ site.baseurl }}/software/fattura-elettronica-reader-2.0.3/release.html)
- [`2.0.2`]({{ site.baseurl }}/software/fattura-elettronica-reader-2.0.2/release.html)
- [`2.0.1`]({{ site.baseurl }}/software/fattura-elettronica-reader-2.0.1/release.html)
- [`2.0.0`]({{ site.baseurl }}/software/fattura-elettronica-reader-2.0.0/release.html)
- [`1.0.0`]({{ site.baseurl }}/software/fattura-elettronica-reader-1.0.0/release.html)

---

### fpydocs

status: active

#### Repository

- [canonical repository](https://software.franco.net.eu.org/frnmst/fpydocs)
- [Codeberg](https://codeberg.org/frnmst/fpydocs)
- [Framagit](https://framagit.org/frnmst/fpydocs)

#### Documentation

- [docs.franco.net.eu.org/fpydocs](https://docs.franco.net.eu.org/fpydocs)

#### Releases

- [`4.1.0`]({{ site.baseurl }}/software/fpydocs-4.1.0/release.html)
- [`4.0.0`]({{ site.baseurl }}/software/fpydocs-4.0.0/release.html)
- [`3.0.0`]({{ site.baseurl }}/software/fpydocs-3.0.0/release.html)
- [`2.0.0`]({{ site.baseurl }}/software/fpydocs-2.0.0/release.html)
- [`1.0.0`]({{ site.baseurl }}/software/fpydocs-1.0.0/release.html)
- [`0.0.1`]({{ site.baseurl }}/software/fpydocs-0.0.1/release.html)

---

### fpyutils

status: active

#### Documentation

- [docs.franco.net.eu.org/fpyutils](https://docs.franco.net.eu.org/fpyutils)

#### Repository

- [canonical repository](https://software.franco.net.eu.org/frnmst/fpyutils)
- [Codeberg](https://codeberg.org/frnmst/fpyutils)
- [Framagit](https://framagit.org/frnmst/fpyutils)
- [GitHub](https://github.com/frnmst/fpyutils)

#### Releases

- [`2.0.1`]({{ site.baseurl }}/software/fpyutils-2.0.1/release.html)
- [`2.0.0`]({{ site.baseurl }}/software/fpyutils-2.0.0/release.html)
- [`1.2.3`]({{ site.baseurl }}/software/fpyutils-1.2.3/release.html)
- [`1.2.2`]({{ site.baseurl }}/software/fpyutils-1.2.2/release.html)
- [`1.2.1`]({{ site.baseurl }}/software/fpyutils-1.2.1/release.html)
- [`1.2.0`]({{ site.baseurl }}/software/fpyutils-1.2.0/release.html)

---

### licheck

status: active

#### Repository

- [canonical repository](https://software.franco.net.eu.org/frnmst/licheck)
- [Codeberg](https://codeberg.org/frnmst/licheck)
- [Framagit](https://framagit.org/frnmst/licheck)

#### Documentation

- [docs.franco.net.eu.org/licheck](https://docs.franco.net.eu.org/licheck)

#### Releases

- [`0.0.3`]({{ site.baseurl }}/software/licheck-0.0.3/release.html)
- [`0.0.2`]({{ site.baseurl }}/software/licheck-0.0.2/release.html)
- [`0.0.1`]({{ site.baseurl }}/software/licheck-0.0.1/release.html)

---

### md-toc

status: active

#### Repository

- [canonical repository](https://software.franco.net.eu.org/frnmst/md-toc)
- [Codeberg](https://codeberg.org/frnmst/md-toc)
- [Framagit](https://framagit.org/frnmst/md-toc)
- [GitHub](https://github.com/frnmst/md-toc)

#### Documentation

- [docs.franco.net.eu.org/md-toc](https://docs.franco.net.eu.org/md-toc)

#### Releases

- [`8.0.1`]({{ site.baseurl }}/software/md-toc-8.0.1/release.html)
- [`8.0.0`]({{ site.baseurl }}/software/md-toc-8.0.0/release.html)
- [`7.2.0`]({{ site.baseurl }}/software/md-toc-7.2.0/release.html)
- [`7.1.0`]({{ site.baseurl }}/software/md-toc-7.1.0/release.html)
- [`7.0.5`]({{ site.baseurl }}/software/md-toc-7.0.5/release.html)
- [`7.0.4`]({{ site.baseurl }}/software/md-toc-7.0.4/release.html)
- [`7.0.3`]({{ site.baseurl }}/software/md-toc-7.0.3/release.html)

---

### monthly-attendance-paper

status: deprecated

#### Repository

- [canonical repository](https://software.franco.net.eu.org/frnmst/monthly-attendance-paper)
- [Codeberg](https://codeberg.org/frnmst/monthly-attendance-paper)

#### Documentation

- [software.franco.net.eu.org/frnmst/monthly-attendance-paper#monthly-attendance-paper](https://software.franco.net.eu.org/frnmst/monthly-attendance-paper#monthly-attendance-paper)

#### Releases

- [`0.0.2`]({{ site.baseurl }}/software/monthly-attendance-paper-0.0.2/release.html)

---

### the-flux-of-thought

status: active

#### Repository

- [canonical repository](https://software.franco.net.eu.org/frnmst/the-flux-of-thought)
- [Codeberg](https://codeberg.org/frnmst/the-flux-of-thought)

#### Documentation

- [software.franco.net.eu.org/frnmst/the-flux-of-thought#the-flux-of-thought](https://software.franco.net.eu.org/frnmst/the-flux-of-thought#the-flux-of-thought)

#### Releases

- [`4.0.0`]({{ site.baseurl }}/software/the-flux-of-thought-4.0.0/release.html)
- [`3.0.0`]({{ site.baseurl }}/software/the-flux-of-thought-3.0.0/release.html)

---

### qvm

status: deprecated

#### Repository

- [canonical repository](https://software.franco.net.eu.org/frnmst/qvm)
- [GitHub](https://github.com/frnmst/qvm)
- [Codeberg](https://codeberg.org/frnmst/qvm)

#### Documentation

- [software.franco.net.eu.org/frnmst/qvm#qvm](https://software.franco.net.eu.org/frnmst/qvm#qvm)

#### Releases

- [`1.0.6`]({{ site.baseurl }}/software/qvm-1.0.6/release.html)
- [`1.0.5`]({{ site.baseurl }}/software/qvm-1.0.5/release.html)
- [`1.0.4`]({{ site.baseurl }}/software/qvm-1.0.4/release.html)
